import rospy
import math
import tf
#from tf.transformations import *
from geometry_msgs.msg import Quaternion, Pose
#from tf.transformations import quaternion_from_euler
#from nav_msgs.msg import Odometry


class TrueAngle(object):

    def __init__(self):
    
        self.is_first_time = True
        self.last_quat_angle = 0
        self.true_angle = 0
        
    def trueAngleDo(self, msg):
    
        print 31
        
        quaternion = (
            msg.x,
            msg.y,
            msg.z,
            msg.w)
        euler = tf.transformations.euler_from_quaternion(quaternion)
        yaw = euler[2]
        #quaternion = msg
        #explicit_quat = [quaternion.x, quaternion.y, quaternion.z, quaternion.w]
        #(roll, pitch, yaw) = quaternion_from_euler(msg.x, msg.y, msg.w)
        #rotation = msg.pose.pose.orientation
        #euler = euler_from_quaternion(quaternion=(rotation.x, rotation.y, rotation.w))
        #curr_quat_angle = msg.w
        curr_quat_angle = yaw
        
        if ( self.is_first_time ):
            self.is_first_time = False
            self.true_angle = 0
            #print 32
            
        else: 
            
            #print 33
            if ((curr_quat_angle - self.last_quat_angle < math.pi) and (curr_quat_angle - self.last_quat_angle > - math.pi)):
            
                #print 34
                self.true_angle = self.true_angle + curr_quat_angle - self.last_quat_angle
                
            elif (curr_quat_angle - self.last_quat_angle > math.pi):
            
                #print 35
                self.true_angle = self.true_angle + (curr_quat_angle - self.last_quat_angle) - 2 * math.pi
                    
            else:
                #print 36
                self.true_angle = self.true_angle + (curr_quat_angle - self.last_quat_angle) + 2 * math.pi
        
        #print 37
        self.last_quat_angle = curr_quat_angle
        print self.true_angle
        return self.true_angle
            
