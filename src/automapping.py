#!/usr/bin/env python

import rospy
import robot
import tf
import math
from math import sqrt
from very_smart_robot.srv import GetGoal, GetGoalResponse, GetGoalRequest
from geometry_msgs.msg import PoseStamped
from nav_msgs.srv import GetPlan 

class Automapping(object):

    def __init__(self):
        rospy.init_node('Automapping', anonymous=True)
        self.robot = robot.Robot()
        self.goal_requester = rospy.ServiceProxy('/get_goal_at_map', GetGoal)
        self.pathfinder = rospy.ServiceProxy('/move_base_node/make_plan', GetPlan)
        
    def distanceBetweenPoses(self, pose1, pose2):
        distance = (pose1.pose.position.x - pose2.pose.position.x) * (pose1.pose.position.x - pose2.pose.position.x) + (pose1.pose.position.y - pose2.pose.position.y) * (pose1.pose.position.y - pose2.pose.position.y)
        return sqrt(distance)  
        
        
    def safeGoalInPlan(self, plan):
        print 5
        goal = PoseStamped()
        goal.pose.orientation.x = 0.0
        goal.pose.orientation.y = 0.0
        goal.pose.orientation.z = 0.0
        goal.pose.orientation.w = 0.0
        last = len(plan.poses) - 1
        i = last - 1
        while (i > 0):
            if(self.distanceBetweenPoses(plan.poses[i], plan.poses[last]) >= 0.01 ):
                goal = plan.poses[i];
                quat = tf.transformations.quaternion_from_euler(0, 0, math.atan2(plan.poses[i+1].pose.position.y - goal.pose.position.y,                                                    plan.poses[i+1].pose.position.x - goal.pose.position.x))
                goal.pose.orientation.x = quat[0]
                goal.pose.orientation.y = quat[1]
                goal.pose.orientation.z = quat[2]
                goal.pose.orientation.w = quat[3]
                return goal
            i = i-1
        return goal
            
        
    def do(self):
        #print 19
        try:
            #print 110
            self.robot.rotateInPlace(370*3.14/180.0, 0.5)
            #print 111
            self.robot.rotateInPlace(-380*3.14/180.0, 0.5)
            #print 112
            client = rospy.ServiceProxy('server0', GetGoal)
            #print 113
            #resp = client()
            #print 114
            #print '\n\nYes, it worked!!!'

        except Exception, e:
            print "Initial Rotation failed: %s"%e
        
        #print 115
        get_goal_srv = GetGoal()
        get_goal_srv.request = GetGoalRequest()
        get_goal_srv.response = GetGoalResponse()
        curr_robot_pose = PoseStamped()
        safe_goal = PoseStamped()  
        get_plan_srv = GetPlan()  
        get_plan_srv.goal = PoseStamped()
        get_plan_srv.goal.header.frame_id = 'map'
        get_plan_srv.tolerance = 0.5
        
        while(True):
            #print 18
            curr_robot_pose = self.robot.currentPose('map')
            get_goal_srv.request.robot_pose = curr_robot_pose.pose.position
            #my_test = GetGoalResponse(curr_robot_pose.pose.position)
            #get_goal_srv.robot_pose = my_test
            
            #print get_goal_srv.request.excluded_goals
            #print my_test
            #self.goal_requester(get_goal_srv.request.robot_pose, get_goal_srv.request.excluded_goals)
            get_goal_response = self.goal_requester(get_goal_srv.request)
            print get_goal_response 
            #self.goal_requester(get_goal_srv.response, get_goal_srv.request)
            print 109
            
            if((get_goal_response.goal.x == curr_robot_pose.pose.position.x) and (get_goal_response.goal.y == curr_robot_pose.pose.position.y)):
                print 11
                
            
            get_plan_srv.start = curr_robot_pose
            get_plan_srv.goal.pose.position = get_goal_response.goal
            print get_plan_srv.goal
            
            resp = self.pathfinder(get_plan_srv.start, get_plan_srv.goal, 0.5)
            
            if(len(resp.plan.poses) == 0):
                get_goal_srv.request.excluded_goals.append(get_goal_srv.response.goal);
                print 12
                continue
            safe_goal = self.safeGoalInPlan(resp.plan)
            if((safe_goal.pose.orientation.x == 0) and (safe_goal.pose.orientation.y == 0) and (safe_goal.pose.orientation.z == 0) and (safe_goal.pose.orientation.w == 0)):
                get_goal_srv.request.excluded_goals.append(get_goal_srv.response.goal);
                print 13
                continue
            
            print "MOOOOOOOOOVE and show me.."
            status = self.robot.moveTo(safe_goal)
            get_goal_srv.request.excluded_goals =  [];
            #print 14

if __name__ == "__main__":
    autoMapping = Automapping()
    autoMapping.do()
    
