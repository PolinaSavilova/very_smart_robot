#!/usr/bin/env python

import rospy

from very_smart_robot.srv import GetGoal, GetGoalResponse, GetGoalRequest
from nav_msgs.srv import GetMap, GetMapRequest, GetMapResponse

class Explorer(object):

    def __init__(self):
        rospy.init_node('Explorer', anonymous=True)
        self.service = rospy.Service('/get_goal_at_map', GetGoal, self.callback)
        self.client = rospy.ServiceProxy("dynamic_map", GetMap)
        
    def processRegion(self, i, j):

        free = 0
        occupied = 0
        unknown = 0
        
        region_size = 11
        total = region_size*region_size
        reg_half = 6

        i = int(i)
        j = int(j)
        
        #print "Pose x"
        #print 
        
        #w 
        #h 
        
        #print i
        #print j
        #print len(m_map.data)
        #print "here"
        #print m_map.info.width

        
                   
        for h in range(i-reg_half, i+reg_half+1):
            
            for w in range(j-reg_half, j+reg_half+1):
                    
                if (m_map.data[w+(m_map.info.width)*h] == -1):
                    unknown = unknown + 1
                    #print w+(m_map.info.width)*h
                elif (m_map.data[w+(m_map.info.width)*h] == 0):
                    free = free + 1
                   # print w+(m_map.info.width)*h
                else:
                    occupied = occupied + 1
                    #print w+(m_map.info.width)*h
                    
        white_num = int(0.65*total)
        grey_num = int(0.3*total)
        black_num = int(0.02*total)  
        if (occupied > black_num):
            return False
        elif ((free > white_num ) and (unknown >= grey_num) and (occupied < 5 )):
            print occupied
            print free
            print unknown
            return True
        return False     
        
    def callback(self, req):
        
        print 41
        srv = GetMap
        srv.response = GetMapResponse()
        #if (self.client.call(srv)):
        #self.client.call(srv)
        
        srv.response = self.client()
        #print 42
        global m_map 
        m_map = srv.response.map
        #print m_map
        for i in range(len(m_map.data)):
            if (m_map.data[i] != -1):
                break
        #else:
        #print 'Some shit happend whith server dynamic_map '
        #print 43
        #req = GetGoalRequest()
        res = GetGoalResponse()
        #print m_map.header
        #print m_map.info
        cur_j = round((req.robot_pose.x - m_map.info.origin.position.x)/m_map.info.resolution)
        cur_i = round((req.robot_pose.y - m_map.info.origin.position.y)/m_map.info.resolution)
        initial_radius = 1.5
        init_radius_in_cells = round(initial_radius/m_map.info.resolution)
        if (init_radius_in_cells%2 != 1):
            #print 44
            init_radius_in_cells = init_radius_in_cells + 1
        exp_j = cur_j - round(init_radius_in_cells/2)   
        exp_i = cur_i - round(init_radius_in_cells/2) - 1
        
        k = 0
        m = int(init_radius_in_cells)
        n = int(init_radius_in_cells) + 1
        exp_i = int(exp_i)
        exp_j = int(exp_j)
        #print n
        
        half = round(init_radius_in_cells/2)
        while(((exp_j - half >= 0) and (exp_j + half < m_map.info.width))  or
         ((exp_i - n - half >= 0) and (exp_i - n + half < m_map.info.height)) or
         ((exp_j - half + m >= 0) and (exp_j + m + half < m_map.info.width)) or
         ((exp_i - half + 1 >= 0) and (exp_i + half + 1 < m_map.info.height))):
         
            
            
            
            
            
            
            
            if((exp_i - half >= 0) and (exp_i + half < m_map.info.height)):
                m = m+1
                print "right"
                for k in range(1,m+1):
                    exp_j = exp_j + 1
                    if (((exp_j - half) < 0) or ((exp_j + half) >= m_map.info.width)):
                        continue
                    if(self.processRegion(exp_i, exp_j)):
                        x = self.x_from_j(exp_j)
                        y = self.y_from_i(exp_i)
                        z = 0
                        for z in range(len(req.excluded_goals)):
                           if(((x-req.excluded_goals[z].x)*(x-req.excluded_goals[z].x) + (y-req.excluded_goals[z].y)*(y-req.excluded_goals[z].y)) < 0.1*0.1):
                                break
                        if (z == len(req.excluded_goals)):
                            res.goal.x = x
                            res.goal.y = y
                            return res
                            
                
            else:
                m = m+1
                exp_j = exp_j + m
                
            
            
            
            
            
            
            if((exp_j - half >= 0) and (exp_j + half < m_map.info.width)):
                
                n = n+1
                print "up"
                for k in range(1,n+1):
                    
                    exp_i = exp_i + 1
                    if (((exp_i - half) < 0) or ((exp_i + half) >= m_map.info.height)):
                        continue
                    if(self.processRegion(exp_i, exp_j)):
                        x = self.x_from_j(exp_j)
                        y = self.y_from_i(exp_i)
                        z = 0
                        for z in range(len(req.excluded_goals)):
                            if(((x-req.excluded_goals[z].x)*(x-req.excluded_goals[z].x) + (y-req.excluded_goals[z].y)*(y-req.excluded_goals[z].y)) < 0.1*0.1):
                                break
                        if (z == len(req.excluded_goals)):
                            res.goal.x = x
                            res.goal.y = y
                            return res
            else:
                n = n+1
                exp_i = exp_i + n
                
            
            
            
            
            
            
            if((exp_i - half >= 0) and (exp_i + half < m_map.info.height)):
                
                m = m + 1
                print "left"
                for k in range(1, m+1):
                    
                    exp_j = exp_j - 1
                    if (((exp_j - half) < 0) or ((exp_j + half) >= m_map.info.width)):
                        continue
                    if(self.processRegion(exp_i, exp_j)):
                        x = self.x_from_j(exp_j)
                        y = self.y_from_i(exp_i)
                        z = 0
                        for z in range(len(req.excluded_goals)):
                            if(((x-req.excluded_goals[z].x)*(x-req.excluded_goals[z].x) + (y-req.excluded_goals[z].y)*(y-req.excluded_goals[z].y)) < 0.1*0.1):
                                break
                        if (z == len(req.excluded_goals)):
                            res.goal.x = x
                            res.goal.y = y
                           
                            return res
            else:
                m = m+1
                exp_j = exp_j - m
                
            
            if((exp_j - half >= 0) and (exp_j + half < m_map.info.width)):
                print "down"
                n = n+1
                for k in range(1,n+1):
                    exp_i = exp_i - 1
                    if (((exp_i - half) < 0) or ((exp_i + half) >= m_map.info.height)):
                        continue
                    
                    if(self.processRegion(exp_i, exp_j)):
                        x = self.x_from_j(exp_j)
                        y = self.y_from_i(exp_i)
                        z = 0
                        for z in range(len(req.excluded_goals)):
                            if(((x-req.excluded_goals[z].x)*(x-req.excluded_goals[z].x) + (y-req.excluded_goals[z].y)*(y-req.excluded_goals[z].y)) < 0.1*0.1):
                                break
                        if (z == len(req.excluded_goals)):
                            
                            res.goal.x = x
                            res.goal.y = y
                            return res
            else:
                n = n+1
                exp_i = exp_i - n
                
                
        res.goal.x = req.robot_pose.x
        res.goal.y = req.robot_pose.y
        return res
        
    def x_from_j(self, j):
        return m_map.info.resolution*j + m_map.info.origin.position.x
        
    def y_from_i(self, i):
        return m_map.info.resolution*i + m_map.info.origin.position.y
        
             
        

if __name__ == "__main__":
    explorer = Explorer()
    #explorer.client.wait_for_service
    rospy.spin()
