import rospy
import actionlib
import tf
from TrueAngle import TrueAngle
from geometry_msgs.msg import Twist, PoseStamped, TransformStamped
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from std_msgs.msg import Header

class Robot(object):

    def __init__(self):
        #rospy.init_node('Robot', anonymous=True)
        self.pub = rospy.Publisher('/cmd_vel', Twist, queue_size = 10)
        self.client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
        
    def stop(self):
        nullVel = Twist()
        self.pub.publish(nullVel)
        
    
    def rotateInPlace(self, angle, speed):
        #print 21
        vel = Twist()
        
        if(angle > 0):
            speed = speed
        else:
            speed = - speed
        vel.angular.z = speed
        robot_pose = PoseStamped()
        #print 22
        robot_angle = TrueAngle()
        #print 23
        
        #rospy.sleep(2.)
        
        try: 
        
            robot_pose = self.currentPose('/odom')

            #while(abs(robot_angle.trueAngleDo (robot_pose.pose.orientation)) < abs(angle)):
            #while(abs(robot_angle.trueAngleDo()) < abs(angle)):
            while abs (robot_angle.trueAngleDo(robot_pose.pose.orientation)) < abs (angle) :
            
                #print 26
                #print robot_angle.trueAngleDo(robot_pose.pose.orientation)
                #print abs(angle)
                self.pub.publish(vel)
                #print robot_pose
                #rospy.sleep(2.)
                robot_pose = self.currentPose('/odom')
                #print robot_pose
                #print robot_angle.trueAngleDo(robot_pose.pose.orientation)
        
        except Exception, e:
            print 'First_exception'
            print e
                
        self.stop()
                
        
    
    def moveTo(self, goal):
        mb_goal = MoveBaseGoal()
        mb_goal.target_pose = goal
        state = self.client.send_goal(mb_goal) 
        wait = self.client.wait_for_result()
        if not wait:
            rospy.logerr("Action server not available!")
            rospy.signal_shutdown("Action server not available!")
        else:
            self.stop()
            return self.client.get_result() 
        
    def currentPose(self, in_frame):

        '''listener = tf.TransformListener()
        transform1 = TransformStamped()
        
        listener.waitForTransform(in_frame, "/base_link", rospy.Time(0), rospy.Duration(4.0))
        (transform1.tranform.translation, transform1.transform.rotation) = listener.lookupTransform(in_frame, "/base_link", rospy.Time(0))
            
        
        pose = PoseStamped()
        pose.header.frame_id = in_frame
        pose.header.stamp = transform1.header.stamp
        
        print transform1.transform.translation
        
        pose.pose.position.x = transform1.transform.translation[0]
        pose.pose.position.y = transform1.transform.translation[1]
        pose.pose.position.z = transform1.transform.translation[3]
        pose.pose.orientation.x = transform1.transform.rotation[0]
        pose.pose.orientation.y = transform1.transform.rotation[1]
        pose.pose.orientation.z = transform1.transform.rotation[2]
        pose.pose.orientation.w = transform1.transform.rotation[3]'''
        

        #print pose
        
        print 27
        listener = tf.TransformListener()
        transform1 = TransformStamped()
        listener.waitForTransform(in_frame, '/base_link', rospy.Time(0), rospy.Duration(4.0));
        now = rospy.Time.now()
        listener.waitForTransform(in_frame, "/base_link", now, rospy.Duration(4.0));
        (transform1.transform.translation, transform1.transform.rotation) = listener.lookupTransform(in_frame, '/base_link', now)
        #listener.lookupTransform(in_frame, '/base_link', now)
        pose = PoseStamped()
        pose.header.frame_id = in_frame
        #transform1.header.stamp = pose.stamp 
        pose.header.stamp = transform1.header.stamp
        pose.pose.position.x = transform1.transform.translation[0]
        pose.pose.position.y = transform1.transform.translation[1]
        pose.pose.orientation.x = transform1.transform.rotation[0]
        pose.pose.orientation.y = transform1.transform.rotation[1]
        pose.pose.orientation.z = transform1.transform.rotation[2]
        pose.pose.orientation.w = transform1.transform.rotation[3]
        
        return pose 
        

   
       
    
        
