define robot position
(
    # actual size
    size [0.25 0.2 0.2]
    
    # centre of rotation offset
    origin [0.125 0 0 0]
         
    # the shape of robot
    block
    (
        points 6
        point[5] [0 0]
        point[4] [0 0.2]
        point[3] [0.15 0.2]
        point[2] [0.2 0.15]
        point[1] [0.2 0.05]
        point[0] [0.15 0]
        z [0 0.2]
    )

    # positonal things
    drive "omni"
    
    # sensors attached to robot
    topurg()
)

define topurg ranger
(
    sensor
    (
        range [ 0.0  20.0 ]
        fov 180
        samples 300
    )
    # interval_sim 10
    # generic model properties
    color "black"
    size [ 0.050 0.050 0.100 ]
)

define robots_sonars ranger
( 
  # one line for each sonar [xpos ypos zpos heading]
  topurg ( pose [ 0 0 0 0]) 
)

